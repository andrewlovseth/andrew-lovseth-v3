<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="about-statement">
		<div class="wrapper">

			<h1><?php the_field('about_statement'); ?></h1>

		</div>
	</section>

	<section id="work-showcase">
		<div class="wrapper">

			<?php $counter = 1; if(have_rows('work_showcase')): while(have_rows('work_showcase')): the_row(); ?>
			
				<?php $showcaseImage = get_sub_field('image');  ?>

				<?php $post_object = get_sub_field('project'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

					<div class="project project-<?php echo $counter; ?>">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo $showcaseImage['url']; ?>" alt="<?php echo $showcaseImage['alt']; ?>" />

							<div class="info">
								<div class="info-wrapper">
									<h3><?php the_title(); ?></h3>
									<h4><?php the_field('date'); ?></h4>
								</div>
							</div>
						</a>
					</div>
				<?php wp_reset_postdata(); endif; ?>


			<?php $counter++; endwhile; endif; ?>


		</div>
	</section>

	<section id="pitch">
		<div class="wrapper">

			<h3 class="section-header"><?php the_field('pitch_headline'); ?></h3>

			<div class="copy">
				<div class="col">
					<?php the_field('pitch_copy_col_1'); ?>
				</div>

				<div class="col">
					<?php the_field('pitch_copy_col_2'); ?>
				</div>
			</div>

		</div>
	</section>


	<section id="pricing">
		<div class="wrapper">

			<h3 class="section-header"><?php the_field('pricing_headline'); ?></h3>

			<div class="pricing-wrapper">

				<?php if(have_rows('pricing_table')): while(have_rows('pricing_table')): the_row(); ?>
				 
				    <div class="option">
				    	<div class="name">
				    		<h4><?php the_sub_field('name'); ?></h4>
				    	</div>

				    	<div class="features">
				    		<?php the_sub_field('features'); ?>
				    	</div>

				    	<div class="price">
				    		<h4><?php the_sub_field('price'); ?></h4>
				    	</div>
				    </div>

				<?php endwhile; endif; ?>

			</div>


		</div>
	</section>

<?php get_footer(); ?>
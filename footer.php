	<footer class="cover" style="background-image: url(<?php $image = get_field('footer_background', 'options'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>"><img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
				<h4><?php the_field('footer_tagline', 'options'); ?></h4>
			</div>

			<nav>
				<?php if(have_rows('footer_nav', 'options')): while(have_rows('footer_nav', 'options')): the_row(); ?>
				 
				    <a href="<?php the_sub_field('link'); ?>">
				        <span><?php the_sub_field('label'); ?></span>
				    </a>

				<?php endwhile; endif; ?>
			</nav>

			<div class="contact">
				<h3><?php the_field('footer_contact_headline', 'options'); ?></h3>
				<a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a>
			</div>

		</div>
	</footer>


	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-22331135-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-22331135-1');
</script>

</body>
</html>
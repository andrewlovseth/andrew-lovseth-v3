<?php

/*

	Template Name: Links

*/


get_header(); ?>

	<section class="links">
		<div class="wrapper">

			<?php if(have_rows('links')): while(have_rows('links')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'section' ): ?>					
					<section class="link-group" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>">
						<div class="section-header">
							<h2><?php the_sub_field('section_title'); ?></h2>
							<?php if(get_sub_field('section_description')): ?>
								<div class="deck">
									<?php the_sub_field('section_description'); ?>
								</div>					
							<?php endif; ?>
						</div>

						<div class="link-list">
							<?php if(have_rows('link_list')): while(have_rows('link_list')): the_row(); ?>
							 
							    <div class="link">
							    	<div class="anchor">
							    		<a href="<?php the_sub_field('link'); ?>" rel="external"><?php the_sub_field('label'); ?></a>
							    	</div>

							    	<div class="copy">
										<?php if(get_sub_field('description')): ?>
											<?php the_sub_field('description'); ?>
										<?php endif; ?>
							    	</div>
							    </div>

							<?php endwhile; endif; ?>							
						</div>
					</section>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>


<?php get_footer(); ?>
<?php get_header(); ?>

	<section id="blog">
		<div class="wrapper">

			<section id="journal-header">
				<div class="wrapper">
					
					<img src="<?php bloginfo('template_directory'); ?>/images/journal.svg" alt="Journal">

				</div>
			</section>

			<section class="archive">

				<?php
					$last_month = null;
					$last_year = null;
					$display_year = null;
					$display_month = null;

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => -1
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<?php $the_month = get_the_time( 'Y-m' ); if ( $last_month !== $the_month ): ?>
						<?php if($last_month !== null): ?>
							</section></div>
						<?php endif; ?>

						<?php if(get_the_time('Y') !== $display_year): ?>
							<div class="year-header">
								<h4><?php $display_year = get_the_time('Y'); the_time('Y'); ?></h4>
							</div>
						<?php endif; ?>

						<div class="month month-<?php echo $the_month; ?>">
							<aside>
								<?php if(get_the_time('F') !== $display_month): ?>
									<h5 class="month-header"><?php $display_month = get_the_time('F'); the_time('F'); ?></h5>
								<?php endif; ?>		
							</aside>

							<section class="posts">

					<?php endif; $last_month = $the_month; ?>

						<article class="post">
							<div class="info">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

								<div class="date">
									<em><?php the_time('F j, Y'); ?></em>
								</div>
							</div>
						</article>

					<?php if ( $query->current_post + 1 === $query->post_count ): ?>
						</div>
					<?php endif; ?>

				<?php endwhile; endif; wp_reset_postdata(); ?>				

			</section>

		</div>
	</section>

<?php get_footer(); ?>
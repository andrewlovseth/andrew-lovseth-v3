<?php get_header(); ?>

	<section id="work-gallery">
		<div class="wrapper">

			<section id="work-gallery-wrapper">
		
				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

						<article>

							<section class="thumbnail">
								<a href="<?php the_permalink(); ?>">
									<img src="<?php $image = get_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</section>

							<section class="info">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<h4><?php the_field('date'); ?></h4>
							</section>

						</article>

				<?php endwhile; endif; ?>
				
			</section>

		</div>
	</section>


<?php get_footer(); ?>
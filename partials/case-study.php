<article class="case-study">

	<div class="screenshot">

		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('screenshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>

	</div>

	<div class="info">

		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<h3><?php the_field('date'); ?></h3>

		<div class="description">
			<?php the_field('description'); ?>
		</div>

		<?php if(get_field('testimonial')): ?>

			<div class="testimonial">
				<blockquote>
					<?php the_field('testimonial'); ?>
				</blockquote>

				<h5><?php the_field('testimonial_source'); ?></h5>
				<h6><?php the_title(); ?></h6>
			</div>

		<?php endif; ?>

		<div class="capabilities">

			<?php $terms = get_the_terms($post->ID, 'capability'); if($terms): ?>

				<?php foreach($terms as $term): ?>

					<?php $term_link = get_term_link( $term ); ?>

					<a href="<?php echo esc_url($term_link); ?>"><?php echo $term->name; ?></a>

				<?php endforeach; ?>

			<?php endif; ?>

		</div>

	</div>

</article>
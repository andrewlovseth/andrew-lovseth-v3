<div class="article-header">
	<span class="date">
		<span class="month-day"><?php the_time('F j,'); ?></span>
	    <span class="year"><?php the_time('Y'); ?></span>
	</span>
	
	<div class="tags">
	    <?php the_tags( '', '', '' ); ?> 
	</div>

	<h1><?php the_title(); ?></h1>

</div>
<section class="hero">

    <?php $post_object = get_sub_field('post'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

		<article class="post post-hero">
			<div class="image">
				<a href="<?php the_permalink(); ?>">
					<img src="<?php $image = get_field('featured_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="info">
				<h5 class="tag">
					<a href="<?php $term = get_field('primary_tag'); echo get_term_link($term); ?>">
						<?php echo $term->name; ?>
					</a>
				</h5>

				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

				<?php the_field('teaser'); ?>
			</div>
		</article>

	<?php wp_reset_postdata(); endif; ?>

</section>
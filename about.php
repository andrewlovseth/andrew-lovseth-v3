<?php

/*

	Template Name: About

*/


get_header(); ?>

	<section id="about">
		<div class="wrapper">

			<section id="featured-photo">
				<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		
				<div class="caption">
					<p><?php the_field('caption'); ?></p>
				</div>
			</section>

			<section id="biography">
				<?php the_field('biography'); ?>
			</section>

			<section id="contact">
				<h3><?php the_field('footer_contact_headline', 'options'); ?></h3>
				<a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a>
			</section>

		</div>
	</section>


<?php get_footer(); ?>